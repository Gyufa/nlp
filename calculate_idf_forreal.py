import numpy as np
import json as JSON

def calculate(wiki_dict, N):
	wiki_dict_w_idf = {}
	for key in wiki_dict:
		wiki_dict_w_idf[key] = np.log(N/wiki_dict[key])

	return wiki_dict_w_idf

print("Reading nr...")
f = open("doc_nr_ok.txt", "r")
N = int(f.read())
f.close()


print("Reading json...")
with open('wiki_words_lemmantized_ok.json', 'r') as f:
    wiki_dict = JSON.load(f)

print("Calclulating idf...")
wiki_idf = calculate(wiki_dict, N)

print("This is the end...")
json = JSON.dumps(wiki_idf)
filename = "wiki_words_lemmantized_idf.json"
f = open(filename,"w")
f.write(json)
f.close()