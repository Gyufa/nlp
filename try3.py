import xml.etree.ElementTree as etree
import codecs
import csv
import time
import os

PATH_WIKI_XML = '.'
PATH_Articles = './articles2'
FILENAME_WIKI = 'enwiki.xml'
FILENAME_ARTICLES = 'articles2'
FILENAME_TITLES = 'titles'
FILENAME_REDIRECT = 'redirect'
FILENAME_TEXT = 'text'


ENCODING = "utf-8"


# Nicely formatted time string
def hms_string(sec_elapsed):
    h = int(sec_elapsed / (60 * 60))
    m = int((sec_elapsed % (60 * 60)) / 60)
    s = sec_elapsed % 60
    return "{}:{:>02}:{:>05.2f}".format(h, m, s)


def strip_tag_name(t):
    t = elem.tag
    idx = k = t.rfind("}")
    if idx != -1:
        t = t[idx + 1:]
    return t


pathWikiXML = os.path.join(PATH_WIKI_XML, FILENAME_WIKI)
pathArticles = os.path.join(PATH_WIKI_XML, FILENAME_ARTICLES)
pathTitles = os.path.join(PATH_WIKI_XML, FILENAME_TITLES)
pathRedirect = os.path.join(PATH_WIKI_XML, FILENAME_REDIRECT)
pathText = os.path.join(PATH_WIKI_XML, FILENAME_TEXT)


totalCount = 0
articleCount = 0
title = None
start_time = time.time()


f= open(pathTitles+"/titles"+str(totalCount)+".csv","w",encoding=ENCODING) 
g= open(pathRedirect+"/redirect"+str(totalCount)+".csv","w",encoding=ENCODING)    
h= open(pathText+"/text"+str(totalCount)+".csv","w",encoding=ENCODING)    
f.write("NEWFILE\n")
g.write("NEWFILE\n")
h.write("NEWFILE\n")


for event, elem in etree.iterparse(pathWikiXML, events=('start', 'end')):
    tname = strip_tag_name(elem.tag)

    if event == 'start':
        if tname == 'page':
            text=""
            title = ''
            id = -1
            redirect = ''
            inrevision = False
            ns = 0
        elif tname == 'revision':
            # Do not pick up on revision id's
            inrevision = True
    else:
        if tname == 'title':
            title = elem.text
        elif tname == 'id' and not inrevision:
            id = int(elem.text)
        elif tname == 'redirect':
            redirect = elem.attrib['title']
        elif tname == 'ns':
            ns = int(elem.text)
        elif tname == "text":
            text=elem.text
        elif tname == 'page':
            totalCount += 1
            if text is None:
                text="&$&\n"+"NONETYPE"+"\n&#&"
            else:
                text="&$&\n"+text+"\n&#&"
            f.write(str(id)+","+title+"\"\n")
            g.write(str(id)+","+redirect+"\"\n")
            h.write(str(id)+","+text+"\"\n")
            f.write("NEWFILE\n")
            g.write("NEWFILE\n")
            h.write("NEWFILE\n")
        

            

            if totalCount > 1 and (totalCount % 50000) == 0:
                print("{:,}".format(totalCount))
                f.close()
                f= open(pathTitles+"/titles"+str(totalCount)+".csv","w",encoding=ENCODING)
                g.close()
                g= open(pathRedirect+"/redirect"+str(totalCount)+".csv","w",encoding=ENCODING)  
                h.close()
                h= open(pathText+"/text"+str(totalCount)+".csv","w",encoding=ENCODING)  
                f.write("NEWFILE\n")
                g.write("NEWFILE\n")
                h.write("NEWFILE\n")

        elem.clear()

elapsed_time = time.time() - start_time
f.close()
g.close()
h.close()
print("Total pages: {:,}".format(totalCount))
print("Elapsed time: {}".format(hms_string(elapsed_time)))
