import nltk 
import sys
from nltk.corpus import wordnet as wn 
from nltk.corpus import wordnet_ic
from nltk.corpus import brown
from nltk.tokenize import word_tokenize
from nltk.util import ngrams
import string
from nltk.corpus import stopwords
import re
# from enchant.checker import SpellChecker
# from nltk.tag.stanford import NERTagger
# import nltk.tag.stanford as stt
# st = stt.NERTagger('stanford-ner/all.3class.distsim.crf.ser.gz', 'stanford-ner/stanford-ner.jar')



# Just to make it a bit more readable
WN_NOUN = 'n'
WN_VERB = 'v'
WN_ADJECTIVE = 'a'
WN_ADJECTIVE_SATELLITE = 's'
WN_ADVERB = 'r'
 
def convert(word, from_pos, to_pos):    

    synsets = wn.synsets(word, pos=from_pos)

    if not synsets:
        return []

    lemmas = []
    for s in synsets:
        for l in s.lemmas():
            if s.name().split('.')[1] == from_pos or from_pos in (WN_ADJECTIVE, WN_ADJECTIVE_SATELLITE) and s.name().split('.')[1] in (WN_ADJECTIVE, WN_ADJECTIVE_SATELLITE):
                lemmas += [l]

    # Get related forms
    derivationally_related_forms = [(l, l.derivationally_related_forms()) for l in lemmas]

    # filter only the desired pos (consider 'a' and 's' equivalent)
    related_noun_lemmas = []

    for drf in derivationally_related_forms:
        for l in drf[1]:
            if l.synset().name().split('.')[1] == to_pos or to_pos in (WN_ADJECTIVE, WN_ADJECTIVE_SATELLITE) and l.synset().name().split('.')[1] in (WN_ADJECTIVE, WN_ADJECTIVE_SATELLITE):
                related_noun_lemmas += [l]

    # Extract the words from the lemmas
    words = [l.name() for l in related_noun_lemmas]
    len_words = len(words)
 
    # Build the result in the form of a list containing tuples (word, probability)
    result = [(w) for w in set(words)]
 
    # return all the possibilities sorted by probability
    return result

def filterupper(text):
    return " ".join([word for word in text.split() if word[0].islower() or word.isupper()])

def checkIfOK(word):
	if len(word) <= 1:
		return 0
	if word[0].isupper() and word[1].islower():
		return 0
	return 1

def get_ngrams(text, n ):
    n_grams = ngrams(word_tokenize(text), n)
    return [ ' '.join(grams) for grams in n_grams]

def get_ands(ngrams):
	ret = []
	for l in ngrams:
		k = l.split(" ")
		if k[1] == 'and':
			ret.append(l)

	return ret

def get_buts(ngrams):
	ret = []
	for l in ngrams:
		k = l.split(" ")
		if k[1] == 'but':
			ret.append(l)

	return ret



# print(convert("death", WN_NOUN, WN_VERB))

# exit(0)


synonymsFirst = [] 
antonymsFirst = [] 
positive = set()
negative = set()
semcor_ic = wordnet_ic.ic('ic-semcor.dat')

sp = ["best", "awesome", "excellent", "euphoric", "amazing"]
sn = ["stupid", "horrible", "terrible", "boring", "poor", "worst"]

lemma_names = []

synonymsFirst = {}
antonymsFirst = {}
for s in sp:
	ok = 0
	for syn in wn.synsets(s):
		if syn.pos() == 'a' or syn.pos() == 'r' or syn.pos() == 's':
			ok = 1
			for l in syn.lemmas(): 
				tmp = l.name()
				if tmp not in sn:
					if tmp not in synonymsFirst:
						synonymsFirst[tmp] = 1
					else:
						synonymsFirst[tmp] += 1
					if l.antonyms(): 
						tmp = l.antonyms()[0].name()
						if tmp not in sp:
							if tmp not in antonymsFirst:
								antonymsFirst[tmp] = 1
							else:
								antonymsFirst[tmp] += 1 
	if ok == 0:
		for syn in wn.synsets(s):
			for l in syn.lemmas(): 
				tmp = l.name()
				if tmp not in sn:
					if tmp not in synonymsFirst:
						synonymsFirst[tmp] = 1
					else:
						synonymsFirst[tmp] += 1
					if l.antonyms(): 
						tmp = l.antonyms()[0].name()
						if tmp not in sp:
							if tmp not in antonymsFirst:
								antonymsFirst[tmp] = 1
							else:
								antonymsFirst[tmp] += 1 


negative = antonymsFirst
positive = synonymsFirst

# exit()
# print(positive)
# print("\n")
# print(negative)
# print("\n")
# print(positive.intersection(negative))


# sys.stdin.read(1)

synonymsFirst = {}
antonymsFirst = {}


for s in sn:
	ok = 0 
	for syn in wn.synsets(s):
		if syn.pos() == 'a' or syn.pos() == 'r' or syn.pos() == 's':
			ok = 1
			# print(syn.name() + " " + str(ok))
			for l in syn.lemmas(): 
				tmp = l.name()
				if tmp not in positive:
					if tmp not in negative:
						negative[tmp] = 1
					else:
						negative[tmp] += 1
					if l.antonyms(): 
						tmp = l.antonyms()[0].name()
						if tmp not in negative:
							if tmp not in positive:
								positive[tmp] = 1
							else:
								positive[tmp] += 1
	if ok == 0:
		print(syn.name() + " " + str(ok))
		for syn in wn.synsets(s):
			for l in syn.lemmas(): 
				tmp = l.name()
				if tmp not in positive:
					if tmp not in negative:
						negative[tmp] = 1
					else:
						negative[tmp] += 1
					if l.antonyms(): 
						tmp = l.antonyms()[0].name()
						if tmp not in negative:
							if tmp not in positive:
								positive[tmp] = 1
							else:
								positive[tmp] += 1
				

# print(positive)
# print("\n\n")
# print(negative)

negative_tmp = {}
positive_tmp = {}

for i in range(0, 1): 
	#!!!!!!!!NEGATIBAN
	for s in negative:
		# synonymsSecond = []
		# antonymsSecond = []
		ok = 0 
		for syn in wn.synsets(s):
			for l in syn.lemmas(): 
				tmp = l.name()
				if tmp not in positive and tmp not in positive_tmp:
					if tmp not in negative:
						if tmp not in negative_tmp:
							negative_tmp[tmp] = 1
						else:
							negative_tmp[tmp] += 1
					else:
						negative[tmp] += 1
					if l.antonyms(): 
						tmp = l.antonyms()[0].name()
						if tmp not in negative and tmp not in negative_tmp:
							if tmp not in positive:
								if tmp not in positive_tmp:
									positive_tmp[tmp] = 1
								else:
									positive_tmp[tmp] += 1
							else:
								positive[tmp] += 1 
		# if ok == 0:
		# 	for syn in wn.synsets(s):
		# 		for l in syn.lemmas(): 
		# 				synonymsSecond.append(l.name()) 
		# 				if l.antonyms(): 
		# 					antonymsSecond.append(l.antonyms()[0].name()) 

		# synonymsFirst = synonymsSecond 
		# negative = negative.union(set(synonymsSecond))
		# positive = positive.union(set(antonymsSecond))

	#!!!!!!!!POZITIVBAN
	for s in positive:
		# synonymsSecond = []
		# antonymsSecond = []
		ok = 0 
		for syn in wn.synsets(s):
			for l in syn.lemmas(): 
				tmp = l.name()
				if tmp not in negative:
					if tmp not in positive:
						if tmp not in positive_tmp:
							positive_tmp[tmp] = 1
						else:
							positive_tmp[tmp] += 1
					else:
						positive[tmp] += 1 
					if l.antonyms(): 
						tmp = l.antonyms()[0].name()
						if tmp not in positive:
							if tmp not in negative:
								if tmp not in negative_tmp:
									negative_tmp[tmp] = 1
								else:
									negative_tmp[tmp] += 1
							else:
								negative[tmp] += 1
		
		# negative = negative.union(set(synonymsSecond))
		# positive = positive.union(set(antonymsSecond))

positive.update(positive_tmp)
negative.update(negative_tmp)


for k in list(positive):
	if positive[k] == 1:
		del positive[k]

for k in list(negative):
	if negative[k] == 1:
		del negative[k]


print("POZITIV - {}\n".format(len(positive)))
print(positive) 

print("\n\n\nNEGATIV - {}\n".format(len(negative)))
print(negative)


print("\n\n\n\nCHECK - {}".format(len(set(positive).intersection(set(negative)))))
tmpp = set(positive)
tmpn = set(negative)
print(tmpp.intersection(tmpn))


# exit()

filename = "full_test.txt"
# filename = "texttest.txt"
file = open(filename, "r")
text = file.read()
file.close()


textAlpha = ""
ngrams = get_ngrams(text, 3)
ok = []
opp = []


ands = get_ands(ngrams)
buts = get_buts(ngrams)
print(ands)
print("----------------------------------")
print(buts)
count = 0
ok = 1
positive_tmp = {}
negative_tmp = {}

while ok:
	for l in ands:
		k = l.split(" ")
		if k[0] not in stopwords.words() and k[2] not in stopwords.words() and checkIfOK(k[0]) and checkIfOK(k[2]) and k[0].isalpha() and k[2].isalpha():
			if k[0] in positive or k[2] in positive:
				if k[0] in positive and k[2] not in positive:
					positive[k[0]] += 1
					if k[2] not in positive_tmp:
						positive_tmp[k[2]] = 1
					else:
						positive_tmp[k[2]] += 1
				else:
					if k[2] in positive and k[0] not in positive:
						positive[k[2]] += 1
						if k[0] not in positive_tmp:
							positive_tmp[k[0]] = 1
						else:
							positive_tmp[k[0]] += 1
				
			else:
				if k[0] in negative or k[2] in negative:
					if k[0] in negative and k[2] not in negative:
						negative[k[0]] += 1
						if k[2] not in negative_tmp:
							negative_tmp[k[2]] = 1
						else:
							negative_tmp[k[2]] += 1
					else:
						if k[2] in negative and k[0] not in negative:
							negative[k[2]] += 1
							if k[0] not in negative_tmp:
								negative_tmp[k[0]] = 1
							else:
								negative_tmp[k[0]] += 1
		# else:
		ands.remove(l)


	for l in buts:
		k = l.split(" ")
		if k[0] not in stopwords.words() and k[2] not in stopwords.words() and checkIfOK(k[0]) and checkIfOK(k[2]) and k[0].isalpha() and k[2].isalpha():
			if k[0] in positive and k[2] not in negative:
				positive[k[0]] += 1
				if k[2] not in negative_tmp:
					negative_tmp[k[2]] = 1
				else:
					negative_tmp[k[2]] += 1
			else:
				if k[0] in negative and k[2] not in positive:
					negative[k[0]] += 1
					if k[2] not in positive_tmp:
						positive_tmp[k[2]] = 1
					else:
						positive_tmp[k[2]] += 1
			if k[2] in positive and k[0] not in negative:
				positive[k[2]] += 1
				if k[0] not in negative_tmp:
					negative_tmp[k[0]] = 1
				else:
					negative_tmp[k[0]] += 1
			else:
				if k[2] in negative and k[0] not in positive:
					negative[k[2]] += 1
					if k[0] not in positive_tmp:
						positive_tmp[k[0]] = 1
					else:
						positive_tmp[k[0]] += 1
		# else:
		buts.remove(l)



	positive.update(positive_tmp)
	negative.update(negative_tmp)

	positive_tmp = {}
	negative_tmp = {}

	for i in range(0): 
		#!!!!!!!!NEGATIBAN
		for s in negative:
			ok = 0 
			for syn in wn.synsets(s):
				for l in syn.lemmas(): 
					tmp = l.name()
					if tmp not in positive and tmp not in positive_tmp:
						if tmp not in negative:
							if tmp not in negative_tmp:
								negative_tmp[tmp] = 1
							else:
								negative_tmp[tmp] += 1
						else:
							negative[tmp] += 1
						if l.antonyms(): 
							tmp = l.antonyms()[0].name()
							if tmp not in negative and tmp not in negative_tmp:
								if tmp not in positive:
									if tmp not in positive_tmp:
										positive_tmp[tmp] = 1
									else:
										positive_tmp[tmp] += 1
								else:
									positive[tmp] += 1 



		#!!!!!!!!POZITIVBAN
		for s in positive:
			# synonymsSecond = []
			# antonymsSecond = []
			ok = 0 
			for syn in wn.synsets(s):
				for l in syn.lemmas(): 
					tmp = l.name()
					if tmp not in negative:
						if tmp not in positive:
							if tmp not in positive_tmp:
								positive_tmp[tmp] = 1
							else:
								positive_tmp[tmp] += 1
						else:
							positive[tmp] += 1 
						if l.antonyms(): 
							tmp = l.antonyms()[0].name()
							if tmp not in positive:
								if tmp not in negative:
									if tmp not in negative_tmp:
										negative_tmp[tmp] = 1
									else:
										negative_tmp[tmp] += 1
								else:
									negative[tmp] += 1
			

	positive.update(positive_tmp)
	negative.update(negative_tmp)



	count += 1
	if count == 5:
		ok = 0

print("POZITIV - {}\n".format(len(positive)))
print(set(positive)) 

print("\n\n\nNEGATIV - {}\n".format(len(negative)))
print(set(negative))

h = open("dictPos.txt", "w")
for w in positive:
	h.write(w + " " + str(positive[w]) + "\n")
h.close()

h = open("dictNeg.txt", "w")
for w in negative:
	h.write(w + " " + str(negative[w]) + "\n")
h.close()

same = set(set(positive).intersection(set(negative)))

h = open("dictPureNeg.txt", "w")
for w in negative:
	if w not in same:
		h.write(w + " " + str(negative[w]) + "\n")
h.close()

h = open("dictPurePos.txt", "w")
for w in positive:
	if w not in same:
		h.write(w + " " + str(positive[w]) + "\n")
h.close()

h = open("same.txt", "w")
for w in same:
	h.write(w + "\n")
h.close()


