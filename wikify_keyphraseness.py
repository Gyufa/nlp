import sys
from langdetect import detect
import enchant
import re
import os,glob
import json as JSON
import string
import xml.etree.ElementTree as etree
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer 
import itertools  
import wikipedia
import operator
from nltk import ngrams
import urllib
import fileinput
import webbrowser
new = 2 # open in a new tab, if possible
from urllib.request import pathname2url


try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

lemmatizer = WordNetLemmatizer()

def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True

def listToDict(lst):
    op = {lst[i]: lst[i + 1] for i in range(0, len(lst), 2)}
    return op

def num_there(s):
    return any(i.isdigit() for i in s)

def cleanhtml(raw_html):
	cleanr = re.compile('<.*?>')
	cleantext = re.sub(cleanr, '', raw_html)

	return cleantext

def check_wiki_page(word):
	string = "https://en.wikipedia.org/wiki/"
	for k in word.split(" "):
		string += k + "_"
	string = string[:-1]
	print(string)
	try:
		exist = urllib.request.urlopen(string).getcode()
		if exist == 200:
			return True
	except:
		return False

def check_if_in_text(ngram_set, wiki_keywords):
	wiki_dict = {}
	for ng in ngram_set:
		for line in ng:
			line = ' '.join(line) 
			if wiki_keywords.get(line) != None and check_wiki_page(line):
				wiki_dict[line] = 1
	# exit()
	print(wiki_dict)
	return wiki_dict

def check_if_in_text_mono(ngram_set, wiki_keywords, wiki_dict):
	for ng in ngram_set:
		for line in ng:
			if wiki_keywords.get(line) != None:
				wiki_dict[line] = 1

	return wiki_dict


def create_final_dict(wiki_dict_sorted, nr):
	string = "https://en.wikipedia.org/wiki/"
	i = 0
	ret_dict = {}
	for key in wiki_dict_sorted:
		string = "https://en.wikipedia.org/wiki/"
		string += re.sub("\s+", "_", key)
		# print(string)
		ret_dict[key] = string
		i += 1 
		if i > nr:
			break

	# return dict(sorted(ret_dict, key=lambda l: len(l[0].split(" ")), reverse=True))
	print(ret_dict)
	return ret_dict

def writehtmlformat(string):
	retstring = "<!DOCTYPE html><html>  <body>  <h1>Wikification</h1><div>\n"
	retstring += string
	retstring += "\n</div></body></html>"

	return retstring

def replace_dict(dic):
	ret = {}
	replacement_text = ""
	for key, value  in dic.items():
		replacement_text = "<a href=\"" + value + "\">" + key + "</a>"
		ret[key] = replacement_text
	return ret

def replace_all(text, dic):
    for i, j in dic.items():
        # print(text)
        # print("--------------------------------------------------")
        # print(str(i) + "\t" + str(j))
        text1 = text
        text = re.sub(r'\s' + str(i) + r'([.,!?()\s])', str(" " + j + r'\1'), text, 1)
    
    return text



if len(sys.argv) != 2:
	print("Usage: [filename]")
	sys.exit(0)

print("Reading wiki keywords...")

# with open('wiki_words_lemmatized_idf.json', 'r') as f:
with open('keywords_keyphraseness_final.json', 'r') as f:
    wiki_dict = JSON.load(f)

# for key, value in wiki_dict.items():
# 	if len(key) == 1:
# 		print(key + "\t" + str(len(key)))	
# exit()
# print(wiki_dict)

print("Reading text file...")
filename = sys.argv[1]

f = open(filename, "r")
text = f.read()
f.close()

text_dict = {}

print("Cleaning text...")
text_tmp = text.lower()
text_tmp = cleanhtml(text_tmp)
text_tmp = re.sub(r'\W+', ' ', text_tmp)
words = text_tmp.split(" ")
firstNr = int((len(words) * 6) / 100) + 1
ngrams_set = set()
# ngram_set = []
# print(text_tmp)
for i in range(2, 4):
	ngrams_set.add(ngrams(text_tmp.split(), i))
	# ngram_set.append(ngrams(text_tmp.split(), i))

# print([" ".join(t) for t in ngrams_set.items()])
# print(list(ngrams_set))

# exit()
text_dict = check_if_in_text(ngrams_set, wiki_dict)

for w in words:
	ngrams_set.add(w)
# print(len(ngrams_set))
# print(ngram_set)
check_if_in_text_mono(ngrams_set, wiki_dict, text_dict)

# print(text_dict)
text_dict_sorted = dict(sorted(text_dict.items(), key=operator.itemgetter(1),reverse=True))
# wiki_dict_sorted.update({"hugh laurie", 1000})
final_dict = create_final_dict(text_dict_sorted, firstNr)
string = writehtmlformat(text)
string = replace_all(string, replace_dict(final_dict))
# print(string)

print("Writing the html...")
f = open("result2.html", "w")
parsed_html = BeautifulSoup(string, features="html.parser")
f.write(string)
f.close()

exit()
# words = tokenize_and_filter_out_stopwords(text_tmp)
# lemmatized_words = lemmatize_words_to_dict(words)

# print("Calculating and sorting idfs...")
# tfidf_dict = calculate_tfidf(wiki_dict, lemmatized_words)
# tfidf_dict_sorted = dict(sorted(tfidf_dict.items(), key=operator.itemgetter(1),reverse=True))

# print("Searching keywords...")
# words = re.findall(r'\w+',text)
# # ITT KICSIT CSALOK, MERT CSAK KESORE JUTOTT ESZEMBE H ELLENORIZZEM H VAN-E WIKI OLDALA
# firstNr = int((len(words) * 8) / 100) + 1
# keywords = list(tfidf_dict_sorted.keys())
