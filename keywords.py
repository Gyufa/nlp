import sys
from langdetect import detect
import enchant
import re
import os,glob
import json as JSON
import string
import xml.etree.ElementTree as etree
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer 
import itertools  
import wikipedia
import operator
from nltk import ngrams
import urllib
import fileinput
import webbrowser
new = 2 # open in a new tab, if possible
from urllib.request import pathname2url

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

def num_there(s):
    return any(i.isdigit() for i in s)

def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True

def check_if_wiki_exists(string):
	tmp_string = "https://en.wikipedia.org/wiki/"
	string = re.sub(r'\s+', '_', string)
	tmp_string += string
	# print(tmp_string + "|")
	try:
		exist = urllib.request.urlopen(tmp_string).getcode()
		if exist == 200:
			return True
	except:
		return False

def clean_keywds(keywds, wiki_keywords):
	for w in keywds:
		w = w[2:len(w)-2]
		if all(x.isalpha() or x.isspace() and isEnglish(x) for x in w): 
			if wiki_keywords.get(w) == None:
				wiki_keywords[w] = 1
			else:
				wiki_keywords[w] += 1
	return wiki_keywords

def check_if_in_text(text, wiki_keywords, wiki_dict):
	# for key in wiki_keywords.keys():
	# 	if text.find(key) >= 0:
	# 		wiki_dict[key] += 1

	for key in wiki_keywords.keys():
		if key in text:
			wiki_dict[key] += 1

	return wiki_dict

def calculate_keyphraseness(wiki_keywords, wiki_dict):
	for key in wiki_keywords.keys():
		wiki_dict[key] = (wiki_keywords[key] + 1)/(wiki_dict[key] + 1)

	return wiki_dict



d = {}
endictionary = enchant.Dict("en_US")

folder_path = './text'
filename = 'test2.csv'
nop = 1
cnt = 1
wiki_keywords = {}
text = ''
for filename in glob.glob(os.path.join(folder_path, '*.csv')):
# if True:
	with open(filename) as fp:
		print(">>>" + str(cnt))
		cnt += 1
		textLine = fp.readline().lower()
		while textLine:
			textLine = fp.readline().lower()
			if textLine != "newfile\n" and textLine != "newfile":
				text += textLine
			elif len(text) > 0:
				text = cleanhtml(text)
				val = set(re.findall(r'(\[\[\w*\s*\w*\s*\w*?\]\])', text))
				wiki_keywords = clean_keywds(val, wiki_keywords)
				text = ''
				print(nop)
				nop += 1
				

json = JSON.dumps(wiki_keywords)
filename = "keywords.json"
f = open(filename,"w")
f.write(json)
f.close()



