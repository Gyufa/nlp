import sys
from langdetect import detect
import enchant
import re
import os,glob
import json as JSON
import string
import xml.etree.ElementTree as etree
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer 



wiki_keywords = [{}]

def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True

def listToDict(lst):
    op = {lst[i]: lst[i + 1] for i in range(0, len(lst), 2)}
    return op

def num_there(s):
    return any(i.isdigit() for i in s)

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

def search_wiki_keywords(text):
	return re.findall('(\[\[.*?\]\])', text)

def clean_keywds(keywds):
	for w in keywds:
		length = len(w)
		if length > 5:
			word = w[2:len-2]
			tmp = w.split(":")
			if len(tmp) > 1:
				w = tmp[1]
				if w.isalnum(): 
					if detect(w) == 'en':
						if wiki_keywords.get(w) == None:
							wiki_keywords[w] = 1
						else:
							wiki_keywords[w] += 1
			else:
				# if detect(k_tmp) == 'en':
				if wiki_keywords.get(w) == None:
					wiki_keywords[w] = 1
				else:
					wiki_keywords[w] += 1

def tokenize_and_filter_out_stopwords(text):
	tokens = word_tokenize(text)
	words = [word for word in tokens if word.isalpha()]
	stop_words = { i : 5 for i in stopwords.words('english') }
	words = set(w for w in words if stop_words.get(w) == None and isEnglish(w))
	# for w in words:
	# 	print(w)
	# 	if stop_words.get(w) == None and isEnglish(w):
	# 		words = w
	return words

def lemmantize_words(words):
	lemmatizer = WordNetLemmatizer()
	stemmed = set()
	for w in words:
		stemmed.add(lemmatizer.lemmatize(w))

	return stemmed



endictionary = enchant.Dict("en_US")
folder_path = './text'
dictionary = {}
dictionary_lemmantized = {}
filename = 'tst.txt'
nop = 1
cnt = 1
for filename in glob.glob(os.path.join(folder_path, '*.csv')):
	print(nop)
	nop += 1
	with open(filename) as fp:
		textLine = fp.readline().lower()
		text = ''
		while textLine:
			textLine = fp.readline().lower()
			if textLine != "newfile\n" and textLine != "newfile":
				text += textLine
			elif len(text) > 0:
				# EGY FILE FELDOLGOZASA
				text = cleanhtml(text)
				words = tokenize_and_filter_out_stopwords(text)
				# EZ ITT IGAZABOL NEM KELLETT, CSAK NEM TUDTAM H FOG V SEM
				for w in words:
					if dictionary.get(w) == None:
						dictionary[w] = 1
					else:
						dictionary[w] += 1
				# NA EZ, EZ KELLETT
				lemmantized_words = lemmantize_words(words)
				for w in lemmantized_words:
					if dictionary_lemmantized.get(w) == None:
						dictionary_lemmantized[w] = 1
					else:
						dictionary_lemmantized[w] += 1
				text = ''
				cnt += 1

json = JSON.dumps(dictionary)
filename = "wiki_words.json"
f = open(filename,"w")
f.write(json)
f.close()

json = JSON.dumps(dictionary_lemmantized)
filename = "wiki_words_lemmatized.json"
f = open(filename,"w")
f.write(json)
f.close()

f = open("doc_nr.txt", "w")
f.write(str(cnt))
f.close()
