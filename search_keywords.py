import sys
from langdetect import detect
import enchant
import re
import os,glob
import json as JSON
import string
import xml.etree.ElementTree as etree
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer 
import itertools  
import wikipedia
import operator
from nltk import ngrams
import urllib
import fileinput
import webbrowser
new = 2 # open in a new tab, if possible
from urllib.request import pathname2url

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

def check_if_in_text(ngram_set, wiki_keywords, wiki_dict):
	for ng in ngram_set:
		# print("|" + ng + "|")
		for line in ng:
			line = ' '.join(line) 
			# print("|" + str(line) + "|")
			if wiki_keywords.get(line) != None:
				# print(line)
				wiki_dict[line] += 1
	# exit()
	return wiki_dict

def check_if_in_text_mono(ngram_set, wiki_keywords, wiki_dict):
	for ng in ngram_set:
		for line in ng:
			if wiki_keywords.get(line) != None:
				wiki_dict[line] += 1

	return wiki_dict

def calculate_keyphraseness(wiki_keywords, wiki_dict):
	for key in wiki_keywords.keys():
		wiki_dict[key] = (wiki_keywords[key] + 1)/(wiki_dict[key] + 1)

	return wiki_dict

def check_if_wiki_exists(string):
	tmp_string = "https://en.wikipedia.org/wiki/"
	string = re.sub(r'\s+', '_', string)
	tmp_string += string
	# print(tmp_string + "|")
	try:
		exist = urllib.request.urlopen(tmp_string).getcode()
		if exist == 200:
			return True
	except:
		return False

print("Reading keywords...")
# with open('keywords_fck.json', 'r') as f:
with open('existing_wiki_keywords.json', 'r') as f:
    wiki_keywords = JSON.load(f)

# for key, value in wiki_keywords.items():
# 	if len(key.split(' ')) > 1:
# 		print(key)
# tupl = ("one giant leap")
# if  wiki_keywords.get(tupl) != None:
# 	print("OKKK")

# exit()

print("Checking keywords...")

# wiki_keywords_new = {}
# Ezt csak, mert hulye vagyok :)
# for key, value in wiki_keywords.items():
# 	if check_if_wiki_exists(key):
# 		wiki_dict[key] = 0
# 		wiki_keywords_new[key] = value

wiki_dict = dict.fromkeys(wiki_keywords.keys(),0)

folder_path = './text'
filename = 'test.csv'
nop = 1
cnt = 1
text = ''
print("Starting stuff...")
for filename in glob.glob(os.path.join(folder_path, '*.csv')):
# if True:
	with open(filename) as fp:
		print(">>>" + str(cnt) + " ---> " + filename)
		cnt += 1
		textLine = fp.readline().lower()
		while textLine:
			textLine = fp.readline().lower()
			
			if textLine != "newfile\n" and textLine != "newfile":
				text += textLine
			elif len(text) > 0:
				# print(text)
				text = cleanhtml(text)
				text = re.sub(r'\W+', ' ', text)
				ngram_set = set()
				mono_set = set()
				for i in range(2, 4):
					ngram_set.add(ngrams(text.split(), i))
				wiki_dict = check_if_in_text(ngram_set, wiki_keywords, wiki_dict)
				words = set(text.split(" "))
				mono_set.add(w for w in words)
				wiki_dict = check_if_in_text_mono(mono_set, wiki_keywords, wiki_dict)
				text = ''
				# print(nop)
				# nop += 1

print("Calculating keyphraseness...")
# print(wiki_dict)
wiki_dict = calculate_keyphraseness(wiki_keywords, wiki_dict)


# print("Sorting results...")
# wiki_dict_sorted = dict(sorted(wiki_dict.items(), key=operator.itemgetter(1),reverse=True))

print("Exporting results...")
json = JSON.dumps(wiki_dict)
filename = "keywords_keyphraseness_final.json"
f = open(filename,"w")
f.write(json)
f.close()

exit()