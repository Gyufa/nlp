import os, glob
from keras.preprocessing import text
from keras.preprocessing.sequence import skipgrams
import operator
import json as JSON

import numpy as np

path = "./documents"
docs = []
for filename in glob.glob(os.path.join(path, '*.*')):
	with open(filename) as f:
		docs.append(f.read())

tokenizer = text.Tokenizer()
tokenizer.fit_on_texts(docs)

word2id = tokenizer.word_index
tmp = tokenizer.word_counts

word_count = dict(sorted(tmp.items(), key=operator.itemgetter(1),reverse=True))
actual_word_count = {key:word_count[key] for key in list(word_count.keys())[100:10100]}

actual_word2id = {}
newid = 0
for key in actual_word_count.keys():
	actual_word2id[key] = newid
	newid += 1

vocab_size = len(actual_word2id)

wids = [[actual_word2id[word] for word in text.text_to_word_sequence(doc) if actual_word2id.get(word) != None] for doc in docs]

skip_grams = [skipgrams(wid, vocabulary_size = vocab_size, window_size = 3) for wid in wids]
pairs, labels = skip_grams[0][0], skip_grams[0][1]


i = 0
count = 1
for wi, wj in pairs:
	if labels[i] == 1:
		pair = np.zeros((2, 1))
		pair[0, 0] = wi
		pair[1, 0] = wj
		np.save('train2/pair_' + str(count) + ".npy", pair)
		count += 1
	i += 1

partition = {'train' : [], 'validation' : []}

partition['train'] = list (range (1, count))
partition['validation'] = list (range (1, 2))

JSON.dump(actual_word2id, open ("train2/word2id.json", 'w'))
JSON.dump(partition, open("train2/partition.json",'w'))
print("end")