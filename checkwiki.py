import wikipediaapi as wa
import json as JSON
import re

en_wiki = wa.Wikipedia('en')

print("Reading keywords...")
with open('keywords.json', 'r') as f:
# with open('keywords_test.json', 'r') as f:
    wiki_keywords = JSON.load(f)


res = {}
k = 1
c = 0
length = len(wiki_keywords)
for key, value in wiki_keywords.items():
	string = re.sub(r'\s+', '_', key)
	page = en_wiki.page(string)
	try:
		tmp = page.fullurl
		res[key] = value
	except:
		# print("MEH")
		c += 1

	print(str(k) + "/" + str(length))
	k += 1

print(len(res))

print("Exporting results...")
json = JSON.dumps(res)
filename = "existing_wiki_keywords.json"
f = open(filename,"w")
f.write(json)
f.close()
