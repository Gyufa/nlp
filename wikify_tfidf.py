import sys
from langdetect import detect
import enchant
import re
import os,glob
import json as JSON
import string
import xml.etree.ElementTree as etree
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer 
import itertools  
import wikipedia
import operator
from nltk import ngrams
import urllib
import fileinput
import webbrowser
new = 2 # open in a new tab, if possible
from urllib.request import pathname2url


try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

lemmatizer = WordNetLemmatizer()

def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True

def listToDict(lst):
    op = {lst[i]: lst[i + 1] for i in range(0, len(lst), 2)}
    return op

def num_there(s):
    return any(i.isdigit() for i in s)

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

def search_wiki_keywords(text):
	return re.findall('(\[\[.*?\]\])', text)

def clean_keywds(keywds):
	for w in keywds:
		length = len(w)
		if length > 5:
			word = w[2:len-2]
			tmp = w.split(":")
			if len(tmp) > 1:
				w = tmp[1]
				if w.isalnum(): 
					if detect(w) == 'en':
						if wiki_keywords.get(w) == None:
							wiki_keywords[w] = 1
						else:
							wiki_keywords[w] += 1
			else:
				if wiki_keywords.get(w) == None:
					wiki_keywords[w] = 1
				else:
					wiki_keywords[w] += 1

def tokenize_and_filter_out_stopwords(text):
	tokens = word_tokenize(text)
	words = [word for word in tokens if word.isalpha()]
	stop_words = { i : 5 for i in stopwords.words('english') }
	words = set(w for w in words if stop_words.get(w) == None and isEnglish(w))

	return words

def lemmatize_words(words):
	lemmatizer = WordNetLemmatizer()
	stemmed = set()
	for w in words:
		stemmed.add(lemmatizer.lemmatize(w))

	return stemmed

def lemmatize_words_to_dict(words):
	stemmed = {}
	for w in words:
		wrd = lemmatizer.lemmatize(w)
		if stemmed.get(wrd) == None:
			stemmed[wrd] = 1
		else:
			stemmed[wrd] += 1

	return stemmed

def calculate_tfidf(wiki_dict, lemmatized_words):
	words_tfidf = {}
	for key in lemmatized_words:
		if wiki_dict.get(key) != None:
			words_tfidf[key] = lemmatized_words[key] * wiki_dict[key]

	return words_tfidf

def list_to_lower(lst):
	tmp = []
	for x in lst:
		tmp.append(x.lower())

def add_links(keywords):
	for k in keywords:
		keywords[k] = wikipedia.page(k).url

def select_keyword_ngrams(ngram_list, keywords, tfidf_dict_sorted):
	ret_list = []
	for lst in ngram_list:
		for k in lst:
			ok = True
			cnt = 0
			for x in k:
				tmp = lemmatizer.lemmatize(x.lower())
				if not keywords.count(tmp) > 0:
					if tfidf_dict_sorted.get(tmp) == None:
						ok = False
						break
				else:
					cnt += 1
			if ok and cnt > 0:
				ret_list.append(k)
			
	return ret_list

def check_ngrams(ngram_list):
	ret_list = {}
	for lst in ngram_list:
		string = "https://en.wikipedia.org/wiki/"
		string2 = ""
		ok = True
		for k in lst:
			string += lemmatizer.lemmatize(k.lower()) + "_"
			string2 += k + " "
		string = string[:-1]
		string2 = string2[:-1]
		try:
			exist = urllib.request.urlopen(string).getcode()
			if exist == 200:
				ret_list[string2] = string
		except:
			tmp = "ok"
	return ret_list

def check_if_wiki_exists(ngram):
	try:
		wikipedia.page(string).url
	except:
		ok = False

def writehtmlformat(string):
	retstring = "<!DOCTYPE html><html>  <body>  <h1>Wikification</h1><div>\n"
	retstring += string
	retstring += "\n</div></body></html>"

	return retstring

def add_words_final_version(words, keywords):
	tmp = {}
	for w in words:
		if keywords.count(lemmatizer.lemmatize(w)) > 0:
			string = "https://en.wikipedia.org/wiki/" + w
			try:
				exist = urllib.request.urlopen(string).getcode()
				if exist == 200:
					tmp[w] = string
			except:
				tmpk = "ok"
			

	return tmp 

def replace_all(text, dic):
    for i, j in dic.items():
        # text = text.replace(i, j)
        # text = re.sub(r'(?<!>)' + str(i) + r'\s', str(j + " "), text)
        # text = re.sub(r'\s' + str(i) + r'(?<!</a>\w\w)', str(" " + j), text)
        text = re.sub(r'\s' + str(i) + r'([.,!?()\s])', str(" " + j + r'\1'), text, 1)
    # print(dic_mono)
    # for i, j in dic_mono.items():
    #     # text = text.replace(i, j)
    #     text = re.sub(r'(?<!>)' + str(i) + r'\s', str(j + " "), text)
    #     text = re.sub(r'\s' + str(i) + r'(?<!<)', str(" " + j), text)


    return text

def replace_dict(dic):
	ret = {}
	replacement_text = ""
	for key, value  in dic.items():
		replacement_text = "<a href=\"" + value + "\">" + key + "</a>"
		ret[key] = replacement_text
	return ret



if len(sys.argv) != 2:
	print("Usage: [filename]")
	sys.exit(0)

print("Reading wiki idf...")
with open('wiki_words_lemmatized_idf.json', 'r') as f:
    wiki_dict = JSON.load(f)

print("Reading text file...")
filename = sys.argv[1]

f = open(filename, "r")
text = f.read()
f.close()

print("Cleaning text...")
text_tmp = text.lower()
text_tmp = cleanhtml(text_tmp)
words = tokenize_and_filter_out_stopwords(text_tmp)
lemmatized_words = lemmatize_words_to_dict(words)

print("Calculating and sorting idfs...")
tfidf_dict = calculate_tfidf(wiki_dict, lemmatized_words)
tfidf_dict_sorted = dict(sorted(tfidf_dict.items(), key=operator.itemgetter(1),reverse=True))

print("Searching keywords...")
words = re.findall(r'\w+',text)
# ITT KICSIT CSALOK, MERT CSAK KESORE JUTOTT ESZEMBE H ELLENORIZZEM H VAN-E WIKI OLDALA
firstNr = int((len(words) * 8) / 100) + 1
keywords = list(tfidf_dict_sorted.keys())
keywords = keywords[:firstNr]

print("Getting possible ngrams and links...")
ngram_list = []
for i in range(2, 4):
	ngram_list.append(ngrams(text.split(), i))
ngram_set = check_ngrams(select_keyword_ngrams(ngram_list, keywords, tfidf_dict_sorted))

ngram_set_mono = add_words_final_version(words, keywords)


print("Creating the html...")
string = writehtmlformat(text)
string = replace_all(string, replace_dict(ngram_set))
string = replace_all(string, replace_dict(ngram_set_mono))

print("Writing the html...")
f = open("result.html", "w")
parsed_html = BeautifulSoup(string, features="html.parser")
f.write(string)
f.close()
