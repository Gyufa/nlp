import os, glob
import json as JSON
import numpy as np
import csv
import json as JSON
from sklearn.metrics import mean_squared_error 
from scipy import spatial
import math

def calculateTF(word, document):
	words = document.lower().split()
	nr = words.count(word.lower())

	return nr/len(words)

def calculateIDF(word, documents):
	nr = 0
	for doc in documents:
		if word.lower() in doc.lower().split():
			nr += 1
	# print(np.log(len(documents)/nr))

	return np.log(len(documents)/nr)

def cosine_similarity2(v1, v2):
	norms = np.linalg.norm(v1) * np.linalg.norm(v2)
	vecs = sum(x * y for x, y in zip(v1, v2))

	if norms == 0:
		return 0

	return vecs/norms

def cosine_similarity3(vector1, vector2):
	denom = np.linalg.norm (vector1) * np.linalg.norm (vector2)
	if (denom == 0.0):
		return 0.0
	return np.dot(vector1, vector2)/denom

def cosine_similarity(vector1, vector2):
	# cosine_similarity_value = 0
	# if np.sqrt(vector1 * vector2) != 0:
	cosine_similarity_value = 1 - spatial.distance.cosine(vector1, vector2)

	if cosine_similarity_value < 0 or math.isnan(cosine_similarity_value): 
		return 0

	return cosine_similarity_value

def get_similarity_bag(word1, word2, documents):
	v1 = []
	v2 = []

	idf1 = calculateIDF(word1, documents)
	idf2 = calculateIDF(word2, documents)

	for doc in documents:
		v1.append(idf1 * calculateTF(word1, doc))
		v2.append(idf2 * calculateTF(word2, doc))

	return cosine_similarity(v1, v2)

def get_similarity_network(word1, word2, word_map, weights):
	v1 = weights[word_map[word1]]
	v2 = weights[word_map[word2]]

	return cosine_similarity(v1, v2)



tmp = []
path = "./documents"
docs = []
for filename in glob.glob(os.path.join(path, '*.*')):
	with open(filename) as f:
		docs.append(f.read())

for doc in docs:
	for w in doc.lower().split():
		tmp.append(w)


wordsim_values = []
bag_values = []
neural_network_values = []
counter = 0
# print(tmp)

weights = np.load('weights.npy')
word_map = JSON.load(open("train2/word2id.json"))

# print(weights)

wordsim_filename = 'combined.csv'
with open(wordsim_filename) as f:
	reader = csv.reader(f, delimiter=',')
	line_count = 0
	for row in reader:
		if line_count != 0:
			# if(row[0] in tmp and row[1] in tmp):
			if(row[0] in word_map and row[1] in word_map):
				wordsim_values.append(float(row[2])/10)
				bag_values.append(get_similarity_bag(row[0], row[1], docs))
				neural_network_values.append(get_similarity_network(row[0], row[1], word_map, weights))
				
				counter += 1
				print("counter - {}\t bag - {:.4f}\t n - {:.4f}\t ws - {:.4f}\n".format(counter, bag_values[-1], neural_network_values[-1], wordsim_values[-1]))
			# else:
			# 	wordsim_values.append(-1)
			# 	bag_values.append(-1)
			# 	neural_network_values.append(-1)
		line_count += 1

with open('wordsim_values.txt', 'w') as f:
	f.write(str(wordsim_values))

with open('bag_values.txt', 'w') as f:
	f.write(str(bag_values))

with open('neural_network_values.txt', 'w') as f:
	f.write(str(neural_network_values))

with open('log.txt', 'w') as f:
	mse = mean_squared_error(wordsim_values, bag_values) 
	print('Mean Squared Error (ws + bag_values) = ', str(mse))
	f.write("Mean Squared Error (ws + bag_values) = {}\n".format(str(mse)))
	mse = mean_squared_error(wordsim_values, neural_network_values) 
	print('Mean Squared Error (ws + neural_network_values) = ', str(mse))
	f.write("Mean Squared Error (ws + neural_network_values) = {}\n".format(str(mse)))
	mse = np.corrcoef(wordsim_values, bag_values)[0,1]
	f.write('corrcoef (ws + bag_values) = {}\n'.format(str(mse)))
	print('corrcoef (ws + bag_values) = ', str(mse))
	mse = np.corrcoef(wordsim_values, neural_network_values)[0,1]
	f.write('corrcoef (ws + neural_network_values) = {}\n'.format(str(mse)))
	print('corrcoef (ws + neural_network_values) = ', str(mse))