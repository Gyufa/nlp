import numpy as np
import nltk
import os
import glob
import string
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import json
from nltk.stem import PorterStemmer 
import re
import nltk
import sys

def filterupper(text):
    return " ".join([word for word in text.split() if word[0].islower() or word.isupper()])

def convert(s): 
  
    # initialization of string to "" 
    new = "" 
  
    # traverse in the string  
    for x in s: 
        new += x  
  
    # return string  
    return new 


ps = PorterStemmer()

contractions = {
"ain't": "am not",
"aren't": "are not",
"can't": "cannot",
"can't've": "cannot have",
"'cause": "because",
"could've": "could have",
"couldn't": "could not",
"couldn't've": "could not have",
"didn't": "did not",
"doesn't": "does not",
"don't": "do not",
"hadn't": "had not",
"hadn't've": "had not have",
"hasn't": "has not",
"haven't": "have not",
"he'd": "he had",
"he'd've": "he would have",
"he'll": "he shall",
"he'll've": "he shall have",
"he's": "he has",
"how'd": "how did",
"how'd'y": "how do you",
"how'll": "how will",
"how's": "how has",
"i'd": "I had",
"i'd've": "I would have",
"i'll": "I shall",
"i'll've": "I shall have",
"i'm": "I am",
"i've": "I have",
"isn't": "is not",
"it'd": "it had",
"it'd've": "it would have",
"it'll": "it shall",
"it'll've": "it shall have",
"it's": "it has",
"let's": "let us",
"ma'am": "madam",
"mayn't": "may not",
"might've": "might have",
"mightn't": "might not",
"mightn't've": "might not have",
"must've": "must have",
"mustn't": "must not",
"mustn't've": "must not have",
"needn't": "need not",
"needn't've": "need not have",
"o'clock": "of the clock",
"oughtn't": "ought not",
"oughtn't've": "ought not have",
"shan't": "shall not",
"sha'n't": "shall not",
"shan't've": "shall not have",
"she'd": "she had",
"she'd've": "she would have",
"she'll": "she shall",
"she'll've": "she shall have",
"she's": "she has",
"should've": "should have",
"shouldn't": "should not",
"shouldn't've": "should not have",
"so've": "so have",
"so's": "so as",
"that'd": "that would",
"that'd've": "that would have",
"that's": "that has",
"there'd": "there had",
"there'd've": "there would have",
"there's": "there has",
"they'd": "they had",
"they'd've": "they would have",
"they'll": "they shall",
"they'll've": "they shall have",
"they're": "they are",
"they've": "they have",
"to've": "to have",
"wasn't": "was not",
"we'd": "we had",
"we'd've": "we would have",
"we'll": "we will",
"we'll've": "we will have",
"we're": "we are",
"we've": "we have",
"weren't": "were not",
"what'll": "what shall",
"what'll've": "what shall have",
"what're": "what are",
"what's": "what has",
"what've": "what have",
"when's": "when has",
"when've": "when have",
"where'd": "where did",
"where's": "where has",
"where've": "where have",
"who'll": "who shall",
"who'll've": "who shall have",
"who's": "who has",
"who've": "who have",
"why's": "why has",
"why've": "why have",
"will've": "will have",
"won't": "will not",
"won't've": "will not have",
"would've": "would have",
"wouldn't": "would not",
"wouldn't've": "would not have",
"y'all": "you all",
"y'all'd": "you all would",
"y'all'd've": "you all would have",
"y'all're": "you all are",
"y'all've": "you all have",
"you'd": "you had",
"you'd've": "you would have",
"you'll": "you shall",
"you'll've": "you shall have",
"you're": "you are",
"you've": "you have",
"ain`t": "am not",
"aren`t": "are not",
"can`t": "cannot",
"can`t've": "cannot have",
"`cause": "because",
"could`ve": "could have",
"couldn`t": "could not",
"couldn`t`ve": "could not have",
"didn`t": "did not",
"doesn`t": "does not",
"don`t": "do not",
"hadn`t": "had not",
"hadn`t`ve": "had not have",
"hasn`t": "has not",
"haven`t": "have not",
"he`d": "he hadld",
"he`d`ve": "he would have",
"he`ll": "he shall",
"he`ll`ve": "he shall have",
"he`s": "he has",
"how`d": "how did",
"how`d`y": "how do you",
"how`ll": "how will",
"how`s": "how has",
"i`d": "I had",
"i`d`ve": "I would have",
"i`ll": "I shall",
"i`ll`ve": "I shall have",
"i`m": "I am",
"i`ve": "I have",
"isn`t": "is not",
"it`d": "it had",
"it`d`ve": "it would have",
"it`ll": "it shall",
"it`ll`ve": "it shall have",
"it`s": "it has",
"let`s": "let us",
"ma`am": "madam",
"mayn`t": "may not",
"might`ve": "might have",
"mightn`t": "might not",
"mightn`t`ve": "might not have",
"must`ve": "must have",
"mustn`t": "must not",
"mustn`t`ve": "must not have",
"needn`t": "need not",
"needn`t`ve": "need not have",
"o`clock": "of the clock",
"oughtn`t": "ought not",
"oughtn`t`ve": "ought not have",
"shan`t": "shall not",
"sha`n`t": "shall not",
"shan`t`ve": "shall not have",
"she`d": "she had",
"she`d`ve": "she would have",
"she`ll": "she shall",
"she`ll`ve": "she shall have",
"she`s": "she has",
"should`ve": "should have",
"shouldn`t": "should not",
"shouldn`t`ve": "should not have",
"so`ve": "so have",
"so`s": "so as",
"that`d": "that would",
"that`d`ve": "that would have",
"that`s": "that has",
"there`d": "there had",
"there`d`ve": "there would have",
"there`s": "there has",
"they`d": "they ha",
"they`d`ve": "they would have",
"they`ll": "they shall",
"they`ll`ve": "they shall have",
"they`re": "they are",
"they`ve": "they have",
"to`ve": "to have",
"wasn`t": "was not",
"we`d": "we had",
"we`d`ve": "we would have",
"we`ll": "we will",
"we`ll`ve": "we will have",
"we`re": "we are",
"we`ve": "we have",
"weren`t": "were not",
"what`ll": "what shall",
"what`ll`ve": "what shall have",
"what`re": "what are",
"what`s": "what has",
"what`ve": "what have",
"when`s": "when has",
"when`ve": "when have",
"where`d": "where did",
"where`s": "where has",
"where`ve": "where have",
"who`ll": "who shall",
"who`ll`ve": "who shall have",
"who`s": "who has",
"who`ve": "who have",
"why`s": "why has",
"why`ve": "why have",
"will`ve": "will have",
"won`t": "will not",
"won`t`ve": "will not have",
"would`ve": "would have",
"wouldn`t": "would not",
"wouldn`t`ve": "would not have",
"y`all": "you all",
"y`all`d": "you all would",
"y`all`d`ve": "you all would have",
"y`all`re": "you all are",
"y`all`ve": "you all have",
"you`d": "you had",
"you`d`ve": "you would have",
"you`ll": "you shall",
"you`ll`ve": "you shall have",
"you`re": "you are",
"you`ve": "you have",
"br" : " "
}

def word_features(word):
	return{'word': word}

# def decide(word, wordsPos, wordsNeg):
# 	if word in wordsPos:
# 		# return wordsPos[word]
# 		print("pos")
# 		return 1
# 	else:
# 		if word in wordsNeg:
# 			# return -1 * wordsNeg[word]
# 			print("neg")
# 			return -9
# 		else:
# 			print("none")
# 			return 0
		# return 0

def decide(word, wordsNeg, wordsPos):
	if word in wordsNeg and word in wordsPos:
		if wordsPos[word]/wordsNeg[word] > 1:
			# print("poztort")
			return wordsPos[word]/wordsNeg[word]
		else:
			# print("NEG")
			# print("negtort")
			return -1 * wordsNeg[word]/wordsPos[word]
	else:
		if word in wordsPos:
			# return wordsPos[word]
			return 10 * wordsPos[word]
		else:
			if word in wordsNeg:
				# return -1 * wordsNeg[word]
				return -10 * wordsNeg[word]
			else:
				return 0
		# return 0

# file = open("dictPurePos.txt", "r")
filename = "dictPos.txt"
inwp,cp = np.loadtxt(filename, dtype={'names': ('word', 'count'),'formats': ('U100', 'i8')}, unpack=True)

filename = "dictNeg.txt"
inwn,cn = np.loadtxt(filename, dtype={'names': ('word', 'count'),'formats': ('U100', 'i8')}, unpack=True)

dicPos = {}
dicNeg = {}
countPos = 0
countNeg = 0
wp = [k[0] for k in nltk.pos_tag(inwp) if k[1] != 'NN']
wn = [k[0] for k in nltk.pos_tag(inwn) if k[1] != 'NN']

# exit()

for i in range(0, len(wp)):
	countPos += cp[i]

for i in range(0, len(wn)):
	countNeg += cn[i]

for i in range(0, len(wp)):
	dicPos[wp[i]] = cp[i]/countPos

for i in range(0, len(wn)):
	dicNeg[wn[i]] = cn[i]/countNeg

dicPosStemmed = {}
dicNegStemmed = {}

lenPos = 0
for w in dicPos:
	w_stemmed = ps.stem(w)
	if w_stemmed not in dicPosStemmed:
		dicPosStemmed[w_stemmed] = dicPos[w]
		lenPos += 1
	else:
		dicPosStemmed[w_stemmed] += dicPos[w]

lenNeg = 0
for w in dicNeg:
	w_stemmed = ps.stem(w)
	if w_stemmed not in dicNegStemmed:
		dicNegStemmed[w_stemmed] = dicNeg[w]
		lenNeg += 1
	else:
		dicNegStemmed[w_stemmed] += dicNeg[w]

# print(str(len(wp)) + "\tvs\t" + str(len(wn)))
if lenPos > lenNeg:
	length = lenNeg
else:
	length = lenPos

print(length)

dicPosSortedAndStemmed = {}
dicNegSortedAndStemmed = {}

count = 1
# print(sorted(dicPos, key=dicPos.get, reverse=True))
for w in sorted(dicPosStemmed, key=dicPosStemmed.get, reverse=True):
	if count < length:
		w_stemmed = w
		dicPosSortedAndStemmed[w_stemmed] = dicPosStemmed[w]
		count += 1
	else:
		break

print(count)

# print("\n\n\n\n" + str(count) + "\n\n\n\n\n")

count = 1
for w in sorted(dicNegStemmed, key=dicNegStemmed.get, reverse=True):
	if count < length:
		w_stemmed = w
		dicNegSortedAndStemmed[w_stemmed] = dicNegStemmed[w]
		count += 1
	else:
		break


# print("\n\n\n\n" + str(count) + "\n\n\n\n\n")
# 	h.write(w + "\t" + str(wordsNegMultiples[w]) + "\t" + str(wordsNegMultiples[w]/wordCounterMultiples) + "\t" + str(-1) + "\n")
# 	dictNeg[w] = "negative"

f = open("posTest.txt", "w")
g = open("negTest.txt", "w")
k = 0
for w in dicPosSortedAndStemmed:
		# f.write(w + "\t" + str(wordsPos[w]) + "\t" + str(wordsPos[w]/wordCounterPos) + "\t" + str(1) + "\n")
	f.write(w + "\t" + str(dicPosSortedAndStemmed[w]) + "\n")

for w in dicNegSortedAndStemmed:
		# f.write(w + "\t" + str(wordsPos[w]) + "\t" + str(wordsPos[w]/wordCounterPos) + "\t" + str(1) + "\n")
	g.write(w + "\t" + str(dicNegSortedAndStemmed[w]) + "\n")		

f.close()
g.close()

# dicPos = file.read()
# file.close()
# dicPosStemmed = []
# for w in dicPos.split("\n"):
# 	dicPosStemmed.append(ps.stem(w))

# wordsPos = {}
# for i in range(len(w)):
# 	wordsPos[w[i]] = n1[i]

# filenameNeg = "dictPureNeg.txt"
# file = open("dictPureNeg.txt", "r")
# dicNeg = file.read()
# file.close()
# dicNegStemmed = []
# for w in dicNeg.split("\n"):
# 	dicNegStemmed.append(ps.stem(w))
# filenamePos = "outputNeg.txt"
# w,c,n1,n2 = np.loadtxt(filenamePos, dtype={'names': ('word', 'count', 'nr', 'nr2'),'formats': ('U100', 'i8', 'f8', 'i8')}, unpack=True)

# wordsNeg = {}
# for i in range(len(w)):
# 	wordsNeg[w[i]] = n1[i]
	# wordsNeg[w[i]] = n1[i]

# print(dicPosStemmed)
# print("=============================")
# print(dicNegStemmed)
# exit()


path = './aclImdb_v1/aclImdb/test/pos'

folders = []

files = [f for f in glob.glob(path + "**/*.txt", recursive=True)]
count = 0
wordCounter = 0
wordCounterMultiples = 0
saved = ""
counter = 0
nc = 0
pc = 0
ncm = 0
pcm = 0

for f in files:
	if counter < 100:
		file = open(f, "r")
		# print(file.read())
		t = file.read()
		text = filterupper(t)

		for word in text.split():
		    if word.lower() in contractions:
		        text = text.replace(word, contractions[word.lower()])

		text = text.lower()
		
		# print(text)
		for c in string.punctuation:
			if c == "<" or c == ">":
				text = text.replace(c," ")
			else:
				text = text.replace(c,"")
		
		tokenized = word_tokenize(text)
		# tokenized = word_tokenize("This movie of ilm was very good")

		tokens = [word for word in tokenized if word.isalpha() and word not in stopwords.words()]
		tags =  nltk.pos_tag(tokens)
		tokenized = ''

		# print(tags)
		# exit()
		
		k = 0
		c = 0
		res = 0
		resMultiple = 0

		invset = set()
		realset = set()

		for i in tags:
			# print(tokens)
			word = i[0]
			if word == 'not' or word == 'no' or (k == 1 and word == 'even') or (k == 1 and word == 'quiet') or word == "or":
				if k == 1 and word != 'even' and word != 'quiet':
					word = ps.stem(word)
					# print(word)
					# print(decide(word, dicNegSortedAndStemmed, dicPosSortedAndStemmed))
					res += -1 * decide(word, dicNegSortedAndStemmed, dicPosSortedAndStemmed)
					invset.add(word)
					k = 0
				else:
					k = 1
			else:
				# if i[1] != 'NN':
				word = ps.stem(word)
				# print(word)
				# print(decide(word, dicNegSortedAndStemmed, dicPosSortedAndStemmed))
				res += decide(word, dicNegSortedAndStemmed, dicPosSortedAndStemmed)
				realset.add(word)

		for x in invset:
			resMultiple += -1 * decide(x, dicNegSortedAndStemmed, dicPosSortedAndStemmed)

		for x in realset:
			resMultiple += decide(x, dicNegSortedAndStemmed, dicPosSortedAndStemmed)

		# sys.stdin.read(1)
		
		# exit()
		# exit()
		# print(res)

		# tokens_without_sw = set()
		# for i in tmp:
		# 	word = i[0]
		# 	if word == 'not' or word == 'no' or (k == 1 and word == 'even') or (k == 1 and word == 'quiet') or word == "or":
		# 		k = 1
		# 	else:
		# 		if k == 1:
		# 			if i[1] == 'JJ':
		# 				word1 = ps.stem(word)
		# 				resMultiple += -1 * decide(word1, wordsNeg, wordsPos)
		# 				tokens_without_sw.add(word1)
		# 			k = 0
		# 		else:
		# 			if i[1] == 'JJ':
		# 				word1 = ps.stem(word)
		# 				resMultiple += decide(word1, wordsNeg, wordsPos)
		# 				tokens_without_sw.add(word1)
			

		# tokens_without_sw = [word for word in tokens if not word in stopwords.words() or word == 'not' or word == 'no' ]
		
		# ok = 1
		# k = 0
		# resMultiple = 0
		# wordsMultpiles = {}
		# for word in tokenized:
		# 	if k == 0:
		# 		if not word in stopwords.words() and word != "br":
		# 			tokens_without_sw.add(word)
		# 			word = ps.stem(word)
		# 			if word not in wordsNegMultiples:
		# 				wordsNegMultiples[word] = 1
		# 			else:
		# 				wordsNegMultiples[word] += 1
		# 		else:
		# 			if word == 'not' or word == 'no':
		# 				k == 1
		# 	else:
		# 		k = 0

		# for word in tokens_without_sw:
		# 	word = ps.stem(word)
		# 	res += decide(word, wordsNeg, wordsPos)




		# print(len(tokens_without_sw))
			
				
		# 	# res += classifier.classify(word_features(word))

		print("\n\n\nresMultiple\n")
		if resMultiple > 0:
			print(str(f) + "\tpositive\t" + str(resMultiple))
			pcm += 1
		else:
			if resMultiple < 0:
				print(str(f) + "\tnegative\t" + str(resMultiple))
				ncm +=1
			else:
				print(str(f) + "\tDUNNO\t" + str(resMultiple))

		if res > 0:
			print(str(f) + "\tpositive\t" + str(res))
			pc += 1
		else:
			if res < 0:
				print(str(f) + "\tnegative\t" + str(res))
				nc += 1
			else:
				print(str(f) + "\tDUNNO\t" + str(res))

		counter += 1
		print(counter)
		

print("Negative in " + str(ncm/100) + "%")
print("Positive in " + str(pcm/100) + "%")

print("Mult Negative in " + str(nc/100) + "%")
print("Mult Positive in " + str(pc/100) + "%")