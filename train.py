import json as JSON
from keras.preprocessing import text
from keras.preprocessing.sequence import skipgrams
import tensorflow
from tensorflow.keras import layers, models
from sklearn.metrics import accuracy_score
from sklearn.datasets import load_svmlight_file
import os, glob

import tensorflow as tf

from tensorflow import keras

# import os
# os.environ['OPENBLAS_NUM-THREADS'] = '25'
import numpy as np

keras = tensorflow.compat.v1.keras

class MySequence(tf.keras.utils.Sequence):
    def __init__(self, X, y, batch_size, word_map):
        self.X, self.y = X, y
        self.batch_size = batch_size
        self.word_map = word_map

    def __len__(self):
        return int(np.ceil(len(self.X) / float(self.batch_size)))

    def one_hot_word(self, word, word_map):
        return [0 if i != word else 1 for i in range(len(word_map))]

    def __getitem__(self, batch_id):
        batch_X = self.X[batch_id * self.batch_size:(batch_id + 1) * self.batch_size]
        batch_y = self.y[batch_id * self.batch_size:(batch_id + 1) * self.batch_size]

        return(np.array([self.one_hot_word(w, self.word_map) for w in batch_X]),
                np.array([self.one_hot_word(w, self.word_map) for w in batch_y]))

partition = JSON.load(open("train2/partition.json"))
word_map = JSON.load(open("train2/word2id.json"))

vocab_size = len(word_map)

x = []
y = []
for i in range(1, len(partition['train'])+1):
    pair = np.load('train2/pair_' + str(i) + '.npy')

    x.append(pair[0, 0])
    y.append(pair[1, 0])
    print(i)



batch_size = 100
seq = MySequence(x, y, batch_size, word_map)

model = models.Sequential()
model.add(layers.Input(shape=(vocab_size)))
dense = layers.Dense(50)
model.add(dense)
model.add(layers.Dense(vocab_size, activation='softmax'))
model.compile(optimizer='adam', loss='categorical_crossentropy')

model.fit_generator(generator=seq, epochs=10)
weights = dense.get_weights()[0]
np.save('weights.npy', weights)